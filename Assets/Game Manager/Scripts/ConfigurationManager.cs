﻿using Assets.Game_Manager.Scripts.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ConfigurationManager
{
    #region Properties

    public static ConfigurationManager Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new ConfigurationManager();
                }

                return instance;
            }
        }
    }

    public event Action OnConfigurationUpdated;

    public int CameraSpeed { get; private set; }

    #endregion

    #region Fields
    private static ConfigurationManager instance = null;
    private static readonly object padlock = new object();
    #endregion

    public ConfigurationManager()
    {
        LoadConfiguration();
    }

    #region Public Methods

    public object GetConfiguration(GameConfigurationEnum configuration)
    {

        switch (configuration)
        {
            case GameConfigurationEnum.CameraSpeed:
                return CameraSpeed;

            default:
                return null;
        }
    }

    public void SetConfiguration(GameConfigurationEnum configuration, object newValue)
    {
        switch (configuration)
        {
            case GameConfigurationEnum.CameraSpeed:
                CameraSpeed = (int)newValue;
                PlayerPrefs.SetInt(GameConfigurationEnum.CameraSpeed.ToString(), (int)newValue);
                break;
        }

        OnConfigurationUpdated?.Invoke();
    }

    #endregion

    #region Private Methods

    private void LoadConfiguration()
    {
        CameraSpeed = PlayerPrefs.GetInt(GameConfigurationEnum.CameraSpeed.ToString(), 15);
    }

    #endregion
}
