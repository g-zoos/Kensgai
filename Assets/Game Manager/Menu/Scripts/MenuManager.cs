﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields

    public Canvas canvas;
    private bool isMenuActive = false;
    private bool InputEnabled = true;

    GameManager gameManager;

    #endregion

    #region Unity events

    private void Start()
    {
        gameManager = gameObject.GetComponentInParent<GameManager>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && InputEnabled)
        {

            isMenuActive = !isMenuActive;

            canvas.gameObject.SetActive(isMenuActive);

            if (isMenuActive)
            {
                gameManager.DisablePlayerInput();
            }
            else
            {
                gameManager.EnablePlayerInput();
            }
        }
    }

    #endregion

    #region Events

    #endregion

    #region Public Methods

    public void EnableInput()
    {
        InputEnabled = true;
    }

    public void DisableInput()
    {
        InputEnabled = false;
    }

    #endregion

    #region Private Methods

    #endregion
}
