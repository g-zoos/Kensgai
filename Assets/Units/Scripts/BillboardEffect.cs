﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardEffect : MonoBehaviour
{

    public GameObject sprite;

    // Start is called before the first frame update
    void Awake()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        sprite.transform.LookAt(transform.position + Camera.main.transform.forward);
    }
}
