using Assets.Game_Manager.Scripts.Enum;
using Assets.Player.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class ControllableUnit : Unit
{

    #region Properties

    public Animator Animator;

    public NavMeshAgent Agent;

    public GameObject MovementDisplayPrefab;

    public GameManager gameManager;

    #endregion

    #region Fields

    private Queue<Vector3> movementQueue;

    private Queue<GameObject> movementQueueDisplay;

    private GameObject currentMovementDisplay;

    private bool isMoving;

    private int floorLayerMask;

    #endregion

    #region Unity Events

    protected override void Awake()
    {
        movementQueue = new Queue<Vector3>();
        movementQueueDisplay = new Queue<GameObject>();
        floorLayerMask = LayerMask.GetMask("Floor");

        Agent.updateRotation = false;

        base.Awake();
    }

    protected override void Update()
    {
        HandleInput();

        CheckArrivedDestination();
        
        SetMovementAnimation();

        base.Update();
    }

    

    private void HandleInput()
    {
        if (gameManager.InputEnabled && this.selected)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Unit hoveredUnit = gameManager.CurrentHoveredUnit;

                if (hoveredUnit?.Hostile == true)
                {
                    this.Attack(hoveredUnit);
                }
                else
                {
                    Move();
                }
            }

            if (Input.GetKeyDown(InputManager.Instance.GetHotkey(ActionsEnum.Stop)))
            {
                Stop();
            }
        }
    }

    #endregion

    #region Public methods

    public virtual void Move()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            AddMoveCommandToQueue();
        }
        else
        {
            MoveToMouse();
        }

    }

    public virtual void Stop()
    {
        Agent.SetDestination(transform.position);

        Destroy(currentMovementDisplay);

        movementQueue.Clear();

        foreach (var queueDisplay in movementQueueDisplay)
        {
            Destroy(queueDisplay);
        }
        movementQueueDisplay.Clear();


    }

    public void Attack(Unit target)
    {
        Debug.Log($"Targeting {target}");
    }

    #endregion

    #region Private Methods

    //Movement

    private void MoveToMouse()
    {
        if (GetMousePosition(out RaycastHit hit))
        {
            SetDestination(hit.point);
        }
    }

    private void SetDestination(Vector3 destination)
    {
        var display = Instantiate(MovementDisplayPrefab, destination, Quaternion.identity);
        SetDestination(destination, display);
    }

    private void SetDestination(Vector3 destination, GameObject display)
    {
        if (currentMovementDisplay != null)
        {
            Destroy(currentMovementDisplay);
        }

        currentMovementDisplay = display;

        Agent.SetDestination(destination);
    }

    private void CheckArrivedDestination()
    {
        if (isMoving && Agent.remainingDistance <= 0.1)
        {
            if (currentMovementDisplay != null)
            {
                Destroy(currentMovementDisplay);
            }

            CheckMovementQueue();
        }
    }

    private void CheckMovementQueue()
    {
        if (movementQueue.Any())
        {
            SetDestination(movementQueue.Dequeue(), movementQueueDisplay.Dequeue());
        }
    }

    private void AddMoveCommandToQueue()
    {
        if (GetMousePosition(out RaycastHit hit))
        {
            movementQueue.Enqueue(hit.point);
            movementQueueDisplay.Enqueue(Instantiate(MovementDisplayPrefab, hit.point, Quaternion.identity));
        }
    }

    private void SetMovementAnimation()
    {
        isMoving = Agent.velocity.sqrMagnitude > 0;
        if (isMoving)
        {
            //TODO: Implement turn rate (look rotation with lerp quaternium?)
            transform.rotation = Quaternion.LookRotation(Agent.velocity.normalized);
        }
        float currentSpeed = Agent.velocity.magnitude / Agent.speed;
        Animator.SetFloat("Velocity", currentSpeed);
    }

    // End movement

    private bool GetMousePosition(out RaycastHit hit)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit, 20, floorLayerMask);
    }

    #endregion

}
