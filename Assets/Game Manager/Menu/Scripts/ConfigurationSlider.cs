﻿using Assets.Game_Manager.Scripts.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConfigurationSlider : MonoBehaviour
{
    public GameConfigurationEnum configuration;


    #region Properties

    #endregion

    #region Fields

    private Slider slider;
    private TMP_InputField input;

    #endregion

    #region Unity events

    void Start()
    {
        slider = gameObject.GetComponentInChildren<Slider>();
        input = gameObject.GetComponentInChildren<TMP_InputField>();

        object currentVal = ConfigurationManager.Instance.GetConfiguration(configuration);

        input.text = currentVal.ToString();

        slider.value = (int)currentVal;

        slider.onValueChanged.AddListener(OnSliderValueChanged);
        input.onValueChanged.AddListener(OnInputValueChanged);
    }

    #endregion

    #region Events

    private void OnSliderValueChanged(float value)
    {
        SetConfigurationValue((int)value);

        SetInputDisplay();
    }

    private void OnInputValueChanged(string value)
    {
        if (string.IsNullOrWhiteSpace(value))
        {
            value = "0";
        }

        if (int.TryParse(value, out int result))
        {
            if (result < 0 || result > 100)
            {
                //removes negative numbers and limits the result to 100
                result = Math.Min(Math.Abs(result), 100);
            }

            //This triggers "OnSliderValueChanged" which updates the configuration value and input display
            slider.value = result;
        }
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void SetConfigurationValue(int value)
    {
        ConfigurationManager.Instance.SetConfiguration(configuration, value);
    }

    private void SetInputDisplay()
    {
        //This does not trigger "OnInputValueChanged" (dunno why)
        input.text = ConfigurationManager.Instance.GetConfiguration(configuration).ToString();
    }

    #endregion

}
