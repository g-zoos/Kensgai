﻿using Assets.Game_Manager.Scripts.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class InputManager
{


    #region Properties

    public static InputManager Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new InputManager();
                }

                return instance;
            }
        }
    }

    #endregion

    #region Fields

    private static InputManager instance = null;
    private static readonly object padlock = new object();

    private readonly Dictionary<ActionsEnum, KeyCode> defaultHotkeys = new Dictionary<ActionsEnum, KeyCode>
        {
            { ActionsEnum.FocusPlayer, KeyCode.Space },
            { ActionsEnum.LockCamera, KeyCode.Y },
            { ActionsEnum.Stop, KeyCode.S }
        };

    private readonly Dictionary<ActionsEnum, KeyCode> hotkeys = new Dictionary<ActionsEnum, KeyCode>();

    #endregion


    public InputManager()
    {
        LoadHotkeys();
    }


    #region Public Methods

    public KeyCode GetHotkey(ActionsEnum playerInput)
    {
        return hotkeys[playerInput];
    }

    public void SetHotkey(ActionsEnum inputToChange, KeyCode newKey)
    {
        hotkeys[inputToChange] = newKey;
        PlayerPrefs.SetString(inputToChange.ToString(), newKey.ToString());
    }

    #endregion

    #region Private Methods

    private void LoadHotkeys()
    {
        foreach (ActionsEnum input in Enum.GetValues(typeof(ActionsEnum)))
        {
            string playerPrefHotkey = PlayerPrefs.GetString(input.ToString());

            if (!Enum.TryParse(playerPrefHotkey, out KeyCode hotkey))
            {
                PlayerPrefs.SetString(input.ToString(), defaultHotkeys[input].ToString());
                hotkey = defaultHotkeys[input];
            }

            hotkeys.Add(input, hotkey);
        }
    }

    #endregion







}
