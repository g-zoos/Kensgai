﻿
using UnityEngine;


namespace Assets.Player.Scripts
{
    public class Unit : MonoBehaviour
    {

        #region Properties 

        public int MaxHealth;

        public int Health;

        public bool Hostile;

        #endregion

        #region Fields

        protected bool selected;

        #endregion

        protected virtual void Start()
        {
            
        }

        protected virtual void Awake()
        {

        }

        protected virtual void Update()
        {

        }


        #region Public Methods

        public virtual void ShowHoverEffect()
        {

        }

        public virtual void HideHoverEffect()
        {

        }

        public virtual void SetSelected()
        {
            selected = true;
        }

        public virtual void SetUnselected()
        {
            selected = false;
        }

        #endregion



    }
}
