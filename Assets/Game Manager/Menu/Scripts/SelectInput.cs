﻿using Assets.Game_Manager.Scripts.Enum;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectInput : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields

    public ActionsEnum Input;

    private TMP_InputField inputField;

    private bool isWaitingForNewKey;

    MenuManager menuManager;

    #endregion

    #region Unity events

    void Start()
    {
        menuManager = gameObject.GetComponentInParent<MenuManager>();

        inputField = gameObject.gameObject.GetComponentInChildren<TMP_InputField>();

        inputField.onSelect.AddListener(OnInputSelected);
        inputField.onDeselect.AddListener(OnInputDeselected);

        SetHotkeyDisplay();
    }

    void OnGUI()
    {
        if (Event.current.isKey && Event.current.type == EventType.KeyDown && isWaitingForNewKey)
        {
            if (Event.current.keyCode != KeyCode.Escape && Event.current.keyCode != KeyCode.None)
            {
                InputManager.Instance.SetHotkey(Input, Event.current.keyCode);
            }

            var eventSystem = EventSystem.current;
            if (!eventSystem.alreadySelecting) eventSystem.SetSelectedGameObject(null);
        }
    }

    #endregion

    #region Events

    private void OnInputSelected(string str)
    {
        inputField.text = "Press new key (esc cancel)";

        isWaitingForNewKey = true;
        menuManager.DisableInput();
    }

    private void OnInputDeselected(string arg0)
    {
        SetHotkeyDisplay();

        isWaitingForNewKey = false;

        StartCoroutine(EnableMenuInput());
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private IEnumerator EnableMenuInput()
    {
        yield return new WaitForEndOfFrame();

        menuManager.EnableInput();
    }

    private void SetHotkeyDisplay()
    {
        inputField.text = InputManager.Instance.GetHotkey(Input).ToString();
    }

    #endregion




}
