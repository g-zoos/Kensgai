﻿using Assets.Game_Manager.Scripts.Enum;
using Assets.Player.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Properties

    public Unit MainUnit;

    public Camera MainCamera { get; private set; }

    public Unit CurrentHoveredUnit { get; private set; }

    public bool InputEnabled { get; private set; }

    public CameraController CameraController;

    #endregion

    #region Fields

    private Unit selectedUnit;

    #endregion

    #region Unity events

    void Awake()
    {
        SelectUnit(MainUnit);
        MainCamera = Camera.main;
        InputEnabled = true;
    }

    private void Start()
    {
    }

    private void Update()
    {
        CheckHoveringUnit();
        HandleInput();
    }

    private void HandleInput()
    {
        if (InputEnabled)
        {

            if (Input.GetMouseButtonDown(0))
            {
                if (CurrentHoveredUnit != null)
                {
                    SelectUnit(CurrentHoveredUnit);
                    Debug.Log("Changed selected unit");
                }
            }

            if (Input.GetKey(InputManager.Instance.GetHotkey(ActionsEnum.FocusPlayer)))
            {
                selectedUnit = MainUnit;
                CameraController.Focus(MainUnit.transform.position);
            }

            if (Input.GetKeyDown(InputManager.Instance.GetHotkey(ActionsEnum.LockCamera)))
            {
                CameraController.ToggleCameraLock(selectedUnit.gameObject);
            }
        }
    }

    #endregion

    #region Events

    #endregion

    #region Public Methods

    public void EnablePlayerInput()
    {
        InputEnabled = true;
    }

    public void DisablePlayerInput()
    {
        InputEnabled = false;
    }

    #endregion

    #region Private Methods

    private void SelectUnit(Unit unit)
    {
        selectedUnit?.SetUnselected();
        selectedUnit = unit;
        selectedUnit.SetSelected();
    }

    private bool CastRayToMousePosition(out RaycastHit hit)
    {
        Ray ray = MainCamera.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit);
    }

    private void CheckHoveringUnit()
    {
        if (CastRayToMousePosition(out RaycastHit hit))
        {

            if (hit.collider.gameObject.tag == "Unit")
            {
                var hovered = hit.collider.gameObject.GetComponent<Unit>();
                if (hovered != CurrentHoveredUnit)
                {
                    Debug.Log("Now hovering unit");
                    CurrentHoveredUnit?.HideHoverEffect();
                    CurrentHoveredUnit = hovered;
                    CurrentHoveredUnit.ShowHoverEffect();
                }
            }
            else
            {
                if (CurrentHoveredUnit != null)
                {
                    Debug.Log("Stopped hovering unit");
                    CurrentHoveredUnit?.HideHoverEffect();
                    CurrentHoveredUnit = null;
                }
            }
        }
    }

    #endregion


}
