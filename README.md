# Kenzgai

Kensgai is a topdown open world RPG

## Dependencies

Unity version: \
`2020.3.6f1`



## Contributing

We use the [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) workflow 
 design for this repository's management. \
Every feature branch must have "feature-" prepended to it's name.

For major changes, please open an [issue](https://gitlab.com/g-zoos/Kensgai/-/issues/new) first to discuss what you would like to change.

Please make sure to update tests as appropriate.

For a merge request to be completed the following must be true:
- One of the required approvers approved the changes.
- The build and test pipeline have been completed successfully.
- The post-merge pipeline must succeed prior to merging 

## Releases
You can find every release in the [releases page](https://gitlab.com/g-zoos/Kensgai/-/releases).