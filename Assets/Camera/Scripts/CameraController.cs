﻿using Assets.Game_Manager.Scripts.Enum;
using Assets.Player.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    #region Properties

    public GameManager gameManager;

    #endregion

    #region Fields

    private bool cameraLocked = false;

    private Vector3 mouse3DownPosition;

    private Camera cam;

    private float ScrollSpeed;

    private float CameraHeight = 10;

    private GameObject lockedObject;
    

    #endregion

    #region Unity events

    private void Start()
    {
        cam = gameObject.GetComponent<Camera>();

        

        GetConfiguration();

        ConfigurationManager.Instance.OnConfigurationUpdated += ConfigurationManager_OnConfigurationUpdated;
    }

    void Update()
    {
        if (gameManager.InputEnabled)
        {
            HandleInput();
        }

        if (cameraLocked)
        {
            Focus(lockedObject.transform.position);
        }

        UpdateCameraHeight();
    }

    #endregion

    #region Events

    private void ConfigurationManager_OnConfigurationUpdated()
    {
        GetConfiguration();
    }

    #endregion

    #region Public Methods


    public void Focus(Vector3 position)
    {
        Vector3 newPosition = transform.position;

        Debug.DrawRay(transform.position, transform.forward * 20, Color.blue);

        if (CastRayToFloor(out RaycastHit hit))
        {
            var distanceFromPlayer = hit.point - position;
            newPosition -= distanceFromPlayer;
            newPosition.y = hit.point.y + CameraHeight;
        }

        transform.position = newPosition;
    }

    public void ToggleCameraLock(GameObject unitToFocus)
    {
        cameraLocked = !cameraLocked;

        lockedObject = cameraLocked ? unitToFocus : null;
    }

    #endregion

    #region Private Methods

    private void GetConfiguration()
    {
        ScrollSpeed = ConfigurationManager.Instance.CameraSpeed;
    }

    private void HandleInput()
    {

        if (Input.GetMouseButton(2))
        {
            CameraDrag();
        }
        else
        {
            ScreenBorderMovement();
        }
    }

    private void CameraDrag()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);


        if (Physics.Raycast(ray, out RaycastHit hit, 20, LayerMask.GetMask("Floor")))
        {
            Vector3 mousePosition = hit.point;
            if (Input.GetMouseButtonDown(2)) //The frame mouse 3 was held down
                mouse3DownPosition = mousePosition;

            var delta = ((mousePosition - mouse3DownPosition) * -1);

            var direction = GetDirectionToStayOnFloor(delta);

            transform.position = transform.position + direction;

        }

    }

    private void UpdateCameraHeight()
    {
        if (CastRayToFloor(out RaycastHit hit))
        {
            var newPosition = transform.position;
            newPosition.y = hit.point.y + CameraHeight;
            transform.position = newPosition;
        }
    }

    private void ScreenBorderMovement()
    {
        float verticalDirection = 0;
        float horizontalDirection = 0;

        if (Input.mousePosition.y >= Screen.height * 0.99)
        {
            verticalDirection = 1;
        }

        if (Input.mousePosition.y <= Screen.height * 0.01)
        {
            verticalDirection = -1;
        }

        if (Input.mousePosition.x >= Screen.width * 0.99)
        {
            horizontalDirection = 1;
        }

        if (Input.mousePosition.x <= Screen.width * 0.01)
        {
            horizontalDirection = -1;
        }

        if (verticalDirection != 0 || horizontalDirection != 0)
        {
            Vector3 vertical = transform.forward * verticalDirection;
            Vector3 horizontal = transform.right * horizontalDirection;

            Vector3 direction = vertical + horizontal;
            direction.y = 0;

            direction = direction.normalized * Time.deltaTime * ScrollSpeed;

            direction = GetDirectionToStayOnFloor(direction);

            transform.Translate(direction, Space.World);

        }
    }

    private Vector3 GetDirectionToStayOnFloor(Vector3 moveDirection)
    {
        if (!CastRayToFloor(out _, moveDirection))
        {
            int iteration = 0;
            float offsetLength = 0;

            Vector3 offset;
            do
            {
                offset = Vector3.zero;
                if (iteration == 0)
                    offsetLength += 0.01f;

                if (moveDirection.x > 0 && moveDirection.z > 0) //moving up (+x | +z), search (-x | -z)
                {
                    switch (iteration)
                    {
                        case 0:
                            offset.x = -offsetLength;
                            break;
                        case 1:
                            offset.z = -offsetLength;
                            break;
                        case 2:
                            offset.x = -offsetLength;
                            offset.z = -offsetLength;
                            break;
                    }
                }
                else if (moveDirection.x < 0 && moveDirection.z < 0) //moving down (-x | -z), search (+x | +z)
                {
                    switch (iteration)
                    {
                        case 0:
                            offset.x = +offsetLength;
                            break;
                        case 1:
                            offset.z = +offsetLength;
                            break;
                        case 2:
                            offset.x = +offsetLength;
                            offset.z = +offsetLength;
                            break;
                    }
                }
                else if (moveDirection.x < 0 && moveDirection.z > 0) //moving left (-x | +z ), search (+x | -z)
                {
                    switch (iteration)
                    {
                        case 0:
                            offset.x = +offsetLength;
                            break;
                        case 1:
                            offset.z = -offsetLength;
                            break;
                        case 2:
                            offset.x = +offsetLength;
                            offset.z = -offsetLength;
                            break;
                    }
                }
                else if (moveDirection.x > 0 && moveDirection.z < 0) //moving right (+x | -z ), search (-x | +z)
                {
                    switch (iteration)
                    {
                        case 0:
                            offset.x = -offsetLength;
                            break;
                        case 1:
                            offset.z = +offsetLength;
                            break;
                        case 2:
                            offset.x = -offsetLength;
                            offset.z = +offsetLength;
                            break;
                    }
                }

                iteration = ++iteration % 3;

                //So there is no infinite loop
                if (offsetLength > 30)
                {
                    return moveDirection;
                }

            } while (!CastRayToFloor(out _, moveDirection + offset));

            moveDirection += offset;

        }

        return moveDirection;
    }

    private bool CastRayToFloor(out RaycastHit hit)
    {
        Debug.DrawRay(transform.position, transform.forward * 15, Color.blue);

        return CastRayToFloor(out hit, Vector3.zero);
    }

    private bool CastRayToFloor(out RaycastHit hit, Vector3 offset)
    {
        Ray ray = new Ray(transform.position + offset, transform.forward);
        return Physics.Raycast(ray, out hit, 20, LayerMask.GetMask("Floor"));
    }
    #endregion

}
